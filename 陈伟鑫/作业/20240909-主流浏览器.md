以下是中国市场主流浏览器及其渲染核心和JS引擎的列表：

| 浏览器名称 | 渲染核心 | JS引擎 |
| ----------- | --------- | ------ |
| Google Chrome | Blink | V8     |
| Mozilla Firefox | Gecko | SpiderMonkey |
| Apple Safari | WebKit | JavaScriptCore |
| Microsoft Edge | Blink | V8     |
| UC浏览器 | 基于Blink/WebKit | V8/JavaScriptCore |
| 360安全浏览器 | 基于Blink/WebKit | V8/JavaScriptCore |

表格中的信息是基于搜索结果的最新数据，并且可能会随着时间和技术的发展而变化。例如，Microsoft Edge在早期版本中使用了EdgeHTML作为渲染核心和Chakra作为JS引擎，但在后来的版本中转向了Blink渲染核心和V8 JS引擎。同样，UC浏览器和360安全浏览器等中国国内的浏览器通常基于开源的Blink或WebKit渲染核心，并可能使用与之兼容的JS引擎。 
